# Sprint-07-plan

- [x] H: invite daotest bot to TEC channel that Gideon wants
- [x] G8-5: bot can detect and store the custom server-specified reactions like the TEC logo (all reactions listed when you add a reaction yourself), log_discord_message pycord
- [x] G0.5-0.1: parse the message id to create url for link to original message in discord
- [x] G0.5-0.1: hardcode a new rule to create messages in TopMessage table whenever the number of reaction unique users exceeds 5
- [x] G0.5-0.5: switch stigsite logo to CommonSenseMakers logo on home page
- [x] G: Only allow admin users to see home page tables (entire app?)
- [x] G0.5-0.5: Parse Author json to contain name#1234 named "AuthorDiscordUserName" fully qualified username
- [x] G0.5-0.1: Swap column names in view of table for GardenMessages and TopMessages so they only see "Author" column and it contains human-readable discord username
- [x] S: check exported GardenMessage table imported into Airtable
- [x] G0.5-0.1: Force export csv of garden message such that it looks in Excel just like it does on the home page
- [x] G0.5-0.1: Force export csv of top message such that it looks in Excel just like it does on the home page
- [x] G0.5-0.1: Add editable text field to GardenMessage table named "twitter handle" and populated with an empty string for new garden messages adds
- [x] G0.5-0.1: low priority: put editable fields first in admin views of TopMessage Table and GardenMessage table (Put it after the id to avoid complication with linking to full data)
- [See below] G: Add field to garden message table contain comma or newline-separated **tags** similar to the unique emojies string field currently in the table
- [x] G2-2: Update Databases
  - [x] Add database field with timestamp of the original authorship of message
        * [x] DiscordMessage - **exists**
        * [x] TopMessage - **added**
        * [x] GardenMessage - **added**
  - [x] Add database field with timestamp of the latest discord message edit
        * [x] DiscordMessage - **exists**
        * [x] TopMessage - **unnecessary, because can't edit in this table**
        * [x] GardenMessage - X
  - [x] Add database field with timestamp of the last export of the csv it should contain the same exact time stamp for all messages in a given export
        * [x] TopMessage - **added**
        * [x] GardenMessage - **added**
- [x] G1-1: Add database export_timestamp (if required) field with timestamp of the last export of the csv it should contain the same exact time stamp for all messages in a given export
- [x] G2-4.5: Every 3 minutes run the update top message table with new messages
  - [x] Added a subprocess.run() to on_reaction_add to run create_leaderboard.sh (create_leaderboard_data commands)
  - [x] Refactored the bot to create/update only the message the reaction was added to if it met the threshold
- [x] G1:1.5: `DEBUG=False` in settings.py to prevent tracebacks from appearing on public web pages (security vulnerability).
  - [x] Set DEBUG=False in .env variable (You can put `DEBUG=""` in your env file to allow you to have debug=bool(env.DEBUG) in settings.py and set it to a nonempty string on your laptop .env file for localhost or staging)
  - [x] Trigger error messages and verify that no traceback appears.
  - [x] You may also need to specify a 404.html and other default error page templates.
  - [x] Added whitenoise to handle static files with DEBUG=False


- [x] G4-5: Update .envs and documentation
    - remove duplicates
    - remove unnecessary
    - make DEBUG .env variable and set false for production
    - Update documentation
      * Add stigapp_rule table
      * Update .env doc example
      * Go through each process and update
      * Update README w/ webhook info
      * Add new database fields
- [x] G0.5-0.1 Add approved admin users

- [x] G8-9.5 - Message type and forum tags (See note below) (Currently on feature-forum-tag branch, not main)
  * [x] Add message_type and forum_tags fields to DiscordMessage, TopMessage, and GardenMessage tables
  * [x] Add message_type and forum_tags to models.py tables
  * [X] Added message_types are Message, Forum, Thread
  * [x] Add message_type/forum_tags to TopMessage creation script
  * [x] Add message_type/forum_tags to TopMessage signal (to create GardenMessage)
  * [x] Add message_type to TopMessage/GardenMessage CSV exports
  * [x] Install py-cord 2.1.3 instead of 2.0.1
  * [x] BUG: Flower reward fails because Thread is not a message. (Check [x] text_channel, [x] text_channel.thread, [x] forum_channel, [x] forum_channel thread)
  * [x] BUG: Duplicate entries in GardenMessage on update
  * [x] BUG: Update GardenMessage table to use user_id instead of test strings.
  * Forum Tag feature not implemented in Py-cord (https://github.com/Pycord-Development/pycord/pull/1636).  Implemented in discord.py (https://discordpy.readthedocs.io/en/latest/api.html?highlight=forum#discord.ForumChannel.available_tags)
