# Sprint 16 (Nov 23 - Nov 30)

- [ ] Make user profile page prettier
- [ ] Make community profile page pretty
- [ ] Community Preferences page (staff) with way to enter twitter handles for each discord handle
- [ ] Community remove menu item for Garden Messages
- [ ] Incorporate threshold rule into community (numerical value)
- [ ] Incorporate threshold rule into community (numerical field (column) name)
- [ ] Incorporate threshold rule into community (numerical field (column) name)

* [] H?: Make Rochdi an administrator on `qary` discord server
* [] R?: Install the project dependencies and create a new branch
* [] R?: Set up new accounts on Airtable and Supabase using my Tangible AI email address
* [] R?: Go to `docs/deploy.md` and copy the `.env` file example
* [] R?: Create a new Supabase database 
* [] R?: Add these [tables](https://gitlab.com/-/ide/project/tangibleai/stigsite/edit/main/-/docs/deploy.md#set-up-supabase-tables) to the newly created Supabase database
* [] R?: Create a new Discord bot on `qary` server
* [] R?: Create a Discord Identity Application for the newly created bot
* [] R?: Create a new Airtable base and add these [fields](https://gitlab.com/-/ide/project/tangibleai/stigsite/edit/main/-/docs/deploy.md#set-up-supabase-tables) to it
* [] R?: Make sure the `.env` file contains the necessary credentials for running `stigsite` properly
* [] R?: Connect the newly created bot to the localhost server for easy testing
* [] R?: Create a new web service to deploy and test the newly created `stigsite` branch 
* [] R?: Add additional Discord accounts to test with different users and settings

