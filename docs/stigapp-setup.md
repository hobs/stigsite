# Stigapp

## Set up a local environment

1) Clone the repository
    ```bash
    git clone https://gitlab.com/tangibleai/stigsite
    ```

2) Download the dependencies in a virtual environment
    ```bash
    python3 -m venv env
    source env/bin/activate
    pip install -r requirements.txt
    ```
    Note: These instructions assume you are using Python 3.9.


3) Create a .env file for the environment variables below
   - Discord Bot
     - PUBLIC_KEY: The key Discord creates for you when you generate a new bot.
     - BOT_TOKEN: The token Discord provides for the bot (Discord Developer Portal > Bot tab)
     - OATH2_CLIENT_ID: The client ID from the OAuth tab (Discord Development Portal > OAuth2)
     - OATH2_CLIENT_SECRET: The Client secret from the Oauth tab (Discord Development Portal > OAuth2)
     - OATH2_GENERATED_URL: The URL created to authorize the Discord bot with the proper permissions (Discord Development Portal > OAuth2)

   - Supabase Database
     - SUPABASE_URL: The url for the project database. (Listed under 'Project Configuration' in Supabase)
     - SUPABASE_KEY: The public key for the project database. (Listed under 'Project API keys' in Supabase)

   - Discord Identity Authentication Application
     - DISCORD_LOGIN_CLIENT_ID: The identity application for logging in with Discord
     - DISCORD_LOGIN_CLIENT_SECRET: The identity application's secret token
     - DISCORD_LOGIN_AUTH_URL: The redirection URL specific to the web application.  For local environments, localhost:8000/oauth2/login/redirect.  For production environments, {URL}/oauth2/login/redirect


4) Create the database 

    ```python
    python manage.py makemigrations
    python manage.py makemigrations stigapp
    python manage.py makemigrations discordlogin
    python manage.py migrate
    python manage.py migrate --run-syncdb
    ```
    Note: The last command may not be necessary depending on your environment.  Ubuntu environments may require this command.


5) Start the server
    ```python
    python manage.py runserver
    ```
    Note: This is only suitable for the local version.  Production versions on a server will use a command from gunicorn, uvicorn, or daphne.


## Project Structure

* **Stigsite:** contains the core settings for the project, including database settings.

* **Stigapp:** a Django application that serves the home page and sets the core database tables (DiscordMessage, DiscordReaction, etc.)
* **log_discord_messages.py:** a separate application (from Django) for running a Discord bot that listens for and extracts interaction data (like messaging and reacting)

* **DiscordLogin:** a Django application that handles user authentication via a Django Identity application

* **Render.com deployment files:**
  - render.yaml: Creates and configures an application
  - buid.sh: Commands to build the environment
  - start.sh: Commands to run the application
  - requirements.txt: A list of all dependencies necessary for the application to run

* **Scripts:** a collection of scripts for setting up data (Currently only has create_leaderboard_data.py)

## References
- [Deployed Application](http://stigsite.onrender.com)
- [Project Repository](https://gitlab.com/tangibleai/stigsite)
- [Discord Developer Portal](https://discord.com/developers/docs/intro)
- [Current Bubble App Data Architecture](https://drive.google.com/file/d/1LOzj7Zhlieez45EEjeAu-n7_rYIfoLSZ/view?usp=sharing)
