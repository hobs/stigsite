# CHANGELOG

## [0.0.14](https://gitlab.com/tangibleai/stigsite/-/tags/0.0.14)

Updates

- Updated navigation bar with condensed menu
- Added relationship between discord_message and site models
- Added form for community profile settings
- Added a capability to turn on/off Twitter handle requests from Stigbot


## [0.0.13](https://gitlab.com/tangibleai/stigsite/-/tags/0.0.13)

New Features

- Stigbot will request Twitter handle from users who have not supplied one yet.
- Added initial support for supporting different versions of Stigsite through the Sites framework
- Stigbot will extract URLs from messages


Updates

- Fixed channel_name field value
- Added Forum/Thread title field


## [0.0.2](https://gitlab.com/tangibleai/stigsite/-/tags/0.0.2)

Updates

- Added authorship_data, guild_name, and channel_name to all tables and Airtables
- Updated reaction threshold to work with **greater than or equal to**
- Updated reaction threshold to work with **0** as a value
- Consolidated stigapp models.py files


## [0.0.1](https://gitlab.com/tangibleai/stigsite/-/tags/0.0.1)

Initial release

- User-defined rule for TopMessage table (threshold and emojies list)
- TopMessage/GardenMessage airtable live (real-time) sync
- BUGFIX: all reactions are monitored using on_raw_reaction_add() and on_raw_reaction_remove()
