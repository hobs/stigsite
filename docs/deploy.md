# Deployment

## Make accounts
Make an account at each of the following sites.
1) [Render](https://render.com/)
2) [Discord](https://discord.com/)
3) [Supabase](https://supabase.com/)
4) [Airtable](https://airtable.com/)

## Make an .env file

The .env file will contains the credentials necessary to use the Discord bot, Discord authentication service, Discord webhook, and Supabase database.  It will also contain Django settings and an Airtable integration experiment.

Throughout the following sections, update the .env file with the relevant values as you create the stigsite components.


#### _`.env`_
```bash
# Supabase database credentials ########
## Used in settings.py
SUPABASE_DOMAIN=
SUPABASE_DB_HOST=db.${SUPABASE_DOMAIN}
SUPABASE_PW=
## Used in log_discord_message.py and loggers.py
SUPABASE_URL=https://${SUPABASE_DOMAIN}
SUPABASE_KEY=

# Discord bot credentials ##############
## Used in log_discord_message.py
DISCORD_BOT_ID=
DISCORD_BOT_TOKEN=

# Discord user authentication ##########
## Used in discordlogin/views.py
DISCORD_LOGIN_AUTH_URL=
DISCORD_LOGIN_CLIENT_ID=
DISCORD_LOGIN_CLIENT_SECRET=

# Discord webhook ######################
## Used in stigapp/signals.py
DISCORD_WEBHOOK=
## Used in log_discord_messages.py
DISCORD_WEBHOOK_CHANNEL_NAME=

# Airtable credentials #################
## Used in stigapp/airtable_integration_supabase.py
## (only a test currently)
AIRTABLE_API_KEY=
AIRTABLE_BASE_ID=
AIRTABLE_TABLE_NAME_TOP_MESSAGES=
AIRTABLE_TABLE_NAME_GARDEN=

# Django Settings ######################
## Used in settings.py
DEBUG=False
TIME_ZONE=  # Ex. US/Pacific
```

## Set up Supabase

### Create a database
1) Sign in to [Supabase ](https://supabase.com/).

2) From the home page, click **New project**.

3) Click **New organization**.

4) Enter in an organization name, such as 'TEC'.  Click **Create organization**.

5) In the additional fields that appear, add a Project name, such as "stigsite.""

6) Add a strong database password.

7) Enter this password in the .env file under "Supabase database credentials" *SUPABASE_PW*.

8) The project configuration data will display.

9) Click **Copy** in the Project API keys box.  Add this key to the .env file under "Supabase database credentials" *SUPABASE_KEY*.


### Set up Supabase tables
You need to manually make the tables below for Stigapp to work.

You can add the tables below by going to the **Table editor** section of your Supabase organization's project.

|Table Name | Description |
|---        |---          |
|auth_user | Users who have logged in with their Discord account |
|auth_user_groups | A dummy table to satisfy Django's delete function |
|auth_user_user_permissions | A dummy table to satisfy Django's delete function |
| community_profile | Settings for a particular community's version of Stigsite |
|discord_message | Messages from a Discord server collected by the log_message_data Discord Bot |
|discord_reaction | Reacts from a Discord server collection by the log_message_data Discord Bot |
| extracted_urls | URLs a user included in their Discord message |
|garden_message | A collection of messages chosen by Gardener's from the **top_message** table |
|log | A record of events in log_message_data Discord Bot |
| reward_reaction_log | Handles management of the reward emoji when a post is added from the top_message table to the garden_message table |
| stigapp_rule | Conditions that determine when and what Stigapp does with particular messages |
|top_message | A collection of messages from **discord_message** that have met threshold conditions |



#### 1) auth_user

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | flags | int8 | NULL |
   | public_flags | int8 | NULL |
   | avatar | varchar | NULL |
   | discord_tag | varchar | NULL |
   | email | varchar | NULL |
   | locale | varchar | NULL |
   | first_name | varchar | NULL |
   | last_name | varchar | NULL |
   | password | varchar | NULL |
   | twitter_handle | varchar | NULL |
   | twitter_username | varchar | NULL |
   | username | varchar | NULL |
   | date_joined | timestamptz | NULL |
   | last_login | timestamptz | NULL |
   | active | bool | NULL |
   | admin | bool | NULL |
   | mfa_enabled | bool | NULL |
   | staff | bool | NULL |
   | requested_twitter_handle | bool | NULL |
   | django_site_id | foreign key (int4) | Foreign key to the django_site table id field |

#### 2) auth_user_groups

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | group_id | int8 | NULL |
   | user_id | int8 | NULL |

#### 3) auth_user_user_permissions

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | permission_id | int8 | NULL |
   | user_id | int8 | NULL |

#### 4) community_profile

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | monitored_channels | int8 | NULL |
   | monitored_guilds | int8 | NULL |
   | promotion_threshold | int8 | NULL |
   | site_style | jsonb | NULL |
   | django_site_id | foreign key (int4) | Foreign key to the django_site table id field |

#### 5) discord_message

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | author | jsonb | NULL |
   | forum_tags | jsonb | NULL |
   | reactions | jsonb | NULL |
   | authorship_datetime | timestamptz | NULL |
   | created_at | timestamptz | now() |
   | edited_at | timestamptz | NULL |
   | saved_at | timestamptz| now() |
   | author_discord_username | varchar | NULL |
   | content | varchar | NULL |
   | channel_name | varchar | NULL |
   | guild_name | varchar | NULL |
   | message_thread_title | varchar | NULL |
   | twitter_handle | varchar | NULL |
   | jump_url | varchar | NULL |
   | message_type | varchar | NULL |
   | extracted_urls_id | foreign key (int8) | Foreign key to the extracted_urls table id field |

#### 6) discord_reaction

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | discord_message | int8 | NULL |
   | message_id | int8 | NULL |
   | user | int8 | NULL |
   | created_at | timestamptz | now() |
   | emoji | text | NULL |
   | previous_reactions | jsonb | NULL |
   | previous_reaction_users | jsonb | NULL |
   | users | jsonb | NULL |

#### 7) extracted_urls

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | url_list | jsonb | NULL |

#### 8) garden_message

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | message_id | int8 | NULL |
   | unique_reaction_count | int8 | NULL |
   | user_count | int8 | NULL |
   | unique_user_count | int8 | NULL |
   | original_message | text | NULL |
   | editable_message | text | NULL |
   | author | jsonb | NULL |
   | counts_by_emoji | jsonb  | NULL |
   | emoji_list | jsonb | NULL |
   | forum_tags | jsonb | NULL |
   | reactions | jsonb | NULL |
   | unique_emoji_list | jsonb | NULL |
   | unique_user_list | jsonb | NULL |
   | user_list | jsonb | NULL |
   | reaction_count | float4 | NULL |
   | authorship_dattime | timestamptz | NULL |
   | created_at | timestamptz | NULL |
   | exported_at | timestamptz | NULL |
   | author_discord_username | varchar | NULL |
   | channel_name | varchar | NULL |
   | guild_name | varchar | NULL |
   | user_id | varchar | NULL |
   | user_list | varchar | NULL |
   | twitter_handle | varchar | NULL |
   | jump_url | varchar | NULL |
   | message_thread_title | varchar | NULL |
   | message_type | varchar | NULL |
   | extracted_urls_id | foreign key (int8) | Foreign key to the extracted_urls table id field |

#### 9) log

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | created_at | timestamptz | now() |
   | json | jsonb | NULL |
   | text | varchar | NULL |
   | log_level | int2 | NULL |

#### 10) reward_reaction_log

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | created_at | timestamptz | now() |
   | message_id | int8 | NULL |
   | reaction_sent | boolean | NULL |

#### 11) stigapp_rule

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | channel_name | varchar | NULL |
   | role | varchar | NULL |
   | rule_name | varchar | NULL |
   | chosen_reaction | jsonb | NULL |
   | threshold | float8 | NULL |
   | activated_status | bool | NULL |

#### 12) top_message

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | message_id | int8 | NULL |
   | unique_reaction_count | int8 | NULL |
   | unique_user_count | int8 | NULL |
   | user_count | int8 | NULL |
   | reaction_count | float4 | NULL |
   | original_message | text | NULL |
   | editable_message | text | NULL |
   | added_to_garden | bool | false |
   | author | jsonb | NULL |
   | counts_by_emoji | jsonb | NULL |
   | emoji_list | jsonb | NULL |
   | forum_tags | jsonb | NULL |
   | reactions | jsonb | NULL |
   | unique_emoji_list | jsonb | NULL |
   | unique_user_list | jsonb | NULL |
   | user_list | jsonb | NULL |
   | authorship_dattime | timestamptz | NULL |
   | created_at | timestamptz | NULL |
   | exported_at | timestamptz | NULL |
   | author_discord_username | varchar | NULL |
   | channel_name | varchar | NULL |
   | guild_name | varchar | NULL |
   | message_thread_title | varchar | NULL |
   | twitter_handle | varchar | NULL |
   | jump_url | varchar | NULL |
   | message_type | varchar | NULL |
   | extracted_urls_id | foreign key (int8) | Foreign key to the extracted_urls table id field |


* **NOTE:** Django will create other tables during set up, including auth_group, auth_group_permissions, django_admin_log, django_content_type, django_migrations, django_session, and django_site.  You will need to verify that these tables are present for the application to work.

* **NOTE:** Stigapp rewards posts with an emoji reaction when the move from the top_message table to the garden_message table. Stigsite and the Discord bot use the database table `reward_reaction_log` to communicate about the reward.  Stigsite will add an entry for a post that should receive the reaction.  Each time a reaction is added, the Discord bot checks any eligible messages in `reward_reaction_log`, issues the emoji, and changes the `reaction_sent` column to True.


### Get a Supabase SSL certificate
You will need to include the contents of this certificate into the Render web hosting service later.

1) Click **Settings** (the gear icon).

2) Click **Database**.

3) Scroll down to the "SSL Connection" section.

4) Click **Download Certificate**.


## Set up the Discord components

### Create a Discord bot
1) Log in to the [Discord Developer Portal](https://discord.com/developers/applications) with your Discord login.

2) Click *New Application*.

3) Give the chatbot a name.  Agree to the Terms of Service.  Click *Create*.

4) Copy the APPLICATION ID.  Put it in the .env file under "Discord bot credentials"  *DISCORD_BOT_ID*.

5) From the sidebar, click **Bot**.

6) Click **Add Bot**.  Then click, **Yes, do it!**.

7) Click **Reset Token**.

8) Click **Yes, do it!**.

9) Click **Copy**.

10) You should put this token in the .env file under "Discord bot credentials" DISCORD_BOT_TOKEN.

11) From the sidebar, click **OAuth2**.

12) Select **URL Generator**.

13) In SCOPES, select **bot**.

14) In BOT PERMISSIONS, select all the checkboxes under TEXT PERMISSIONS.

15) In GENERATED URL, click **Copy**.

16) Visit the URL in your browser.

17) Select the server you want the bot to work in. Click **Continue**.  Then, click **Authorize**.



### Create a Discord identity application

1) Log in to the [Discord Developer Portal](https://discord.com/developers/applications) with your Discord login.

2) Click *New Application*.

3) Give the identity application a name, such as StigIdentity.  Agree to the Terms of Service.  Click *Create*.

4) From the sidebar, click **OAuth2**.

5) Click **Reset Secret**.

6) Click **Yes, do it!**.

7) Click **Copy**.

8) Put the CLIENT SECRET in the .env file under "Discord user authentication" *DISCORD_LOGIN_CLIENT_SECRET*.

9) Under "Client ID", click **Copy**.  Save this value in the .env file under "Discord user authentication" *DISCORD_LOGIN_CLIENT_ID*.

10) Under "Redirects", click **Add Redirect**.

11) Enter the redirect for the server you are working on.  The endpoint should be "/oauth2/login/redirect" (ie., http://localhost:8000/oauth2/login/redirect).  Click **Save Changes** when finished.

12) Select **URL Generator**.

13) In SCOPES, select **identify**.

14) In SELECT REDIRECT URL, select a redirect URL from the dropdown menu (Example: http://localhost:8000/oauth2/login/redirect).

15) Under GENERATED URL, click **Copy**.

16) Save the generated url in the .env as *DISCORD_LOGIN_AUTH_URL*.


## Set up Airtable integration

To connect to your Airtable account, Stigsite needs 4 pieces of information:

- [ ] AIRTABLE_API_KEY
- [ ] AIRTABLE_BASE_ID
- [ ] AIRTABLE_TABLE_NAME_TOP_MESSAGES
- [ ] AIRTABLE_TABLE_NAME_GARDEN

Here's what you need to do to retrieve this information.

1. Log in to [Airtable](https://airtable.com/).
2. Click the profile image.
3. Click **Account**.
4. Under the API section, click **Generate API key**.
5. Copy the API key and paste it next to **AIRTABLE_API_KEY** in a text file (name it `.env` if you like).
7. Go back to your Airtable home page.  Create an Airtable base.
8. The base will have an Airtable URL similar to the following:
https://airtable.com/appbDrod2abrajcNS/tblsdJYFSJclFAHAA/viwDjfshEw4gHDhDUq?blocks=hide
9. Copy the portion of the URL that starts with "app" (ie., "appbDrod2abrajcNS").  Add this to the **AIRTABLE_BASE_ID**.
10. Create a `TopMessage`s table within that base (you can name it whatever you like).
11. Copy the table name in the tab above the table you created.  Add this to the **AIRTABLE_TABLE_NAME_TOP_MESSAGES** variable.
12. Repeat steps 10 and 11 for the `GardenMessage`s table and add your new table name to the variable **AIRTABLE_TABLE_NAME_GARDEN** .

### Airtable Fields
The tables in the base need to include all the fields below.  Capitalization and spacing of each field name matters.

#### TopMessage Table
   | Field Name | Type | Additional Settings |
   |---         |---   |---                  |
   | Author | Single line text | NA |
   | Twitter Handle | Single line text | NA |
   | Guild | Single line text | NA |
   | Channel | Single line text | NA |
   | Message ID | Single line text | NA |
   | Message Type | Single line text | NA |
   | Thread Title | Single line text | NA |
   | Original Message | Long text | NA |
   | Authorship | Date | ISO, Include time, 24 hour, Display time zone |
   | Editable Message | Long text | NA |
   | Edited At | Date | ISO, Include time, 24 hour, Display time zone |
   | All Reactions | Number |  Integer |
   | Unique Reactions | Number | Integer |
   | All Emojis | Single line text | NA |
   | Unique Emojis | Single line text | NA |
   | Totals by Emoji | Single line text | NA |
   | Total Users | Number |  Integer |
   | Unique Users | Number |  Integer |
   | Total User List | Single line text | NA |
   | Unique User List | Single line text | NA |
   | Exported At | Date | ISO, Include time, 24 hour, Display time zone |
   | Jump URL | URL | NA |
   | Added to Garden | Checkbox | NA |

#### GardenMessage Table
   | Field Name | Type | Additional Settings |
   |---         |---   |---                  |
   | Author | Single line text | NA |
   | Twitter Handle | Single line text | NA |
   | Guild | Single line text | NA |
   | Channel | Single line text | NA |
   | Message ID | Single line text | NA |
   | Message Type | Single line text | NA |
   | Thread Title | Single line text | NA |
   | Original Message | Long text | NA |
   | Authorship | Date | ISO, Include time, 24 hour, Display time zone |
   | Editable Message | Long text | NA |
   | Edited At | Date | ISO, Include time, 24 hour, Display time zone |
   | All Reactions | Number |  Integer |
   | Unique Reactions | Number | Integer |
   | All Emojis | Single line text | NA |
   | Unique Emojis | Single line text | NA |
   | Totals by Emoji | Single line text | NA |
   | Total Users | Number |  Integer |
   | Unique Users | Number |  Integer |
   | Total User List | Single line text | NA |
   | Unique User List | Single line text | NA |
   | Exported At | Date | ISO, Include time, 24 hour, Display time zone |
   | Jump URL | URL | NA |

## Set up Render web service
In order to follow these steps, you will need to enter credit card information for the account because we will be using the Blueprint feature.

Using the Blueprint feature, you will be able to spin up new instances of the application with similar settings based on a common Git*** repository.

However, you will still be able to define the features unique to each project, such as .env credentials from above.

1) Log in to [Render](https://render.com/).

2) Click **Blueprints**.

3) Click **New Blueprint Instance**.

4) Enter into the **Public Git repository** field the repository you will be using (ie.,  https://gitlab.com/tangibleai/stigsite).  Click **Continue**.

* **NOTE:** The current application requires hardcoding values for things like approved admin users.  You will need to adjust the constants.py file or the discordlogin/models.py users to your project's needs.

5) For the Service Group Name, enter a project name, such as stigsite.

6) Click **Create New Resources**.

7) Click **Apply**.

8) Render will start to build the application.  However, the application will fail because we need to set up the .env files and build/start commands.  Click on **Dashboard**.

9) Click on the new application.

10) Click on **Environment**.

11) Click **Add Secret File**.

12) For Filename, write .env.  For File contents, copy the contents of the .env you have built throughout this document.

13) Click **Add Secret File** again.

14) For Filename, write secrets_prod-ca-2021.crt (or whatever the name of that file that you downloaded is).  Copy the contents of that file.

15) Click **Save Changes**.

16) Click **Events**.

17) Click **Deploy** in the most current deployment.  This is evident by three dots undulating.

18) When the application successfully deploys, Render will display the word "live" in a green box.  The terminal will display the following:
```
Sep 20 05:16:04 PM  ==> Uploading build...
Sep 20 05:16:31 PM  ==> Build uploaded in 12s
Sep 20 05:16:31 PM  ==> Build successful 🎉
Sep 20 05:16:31 PM  ==> Deploying...
Sep 20 05:17:33 PM  ==> Starting service with 'source ./start.sh'
Sep 20 05:17:33 PM  starting pycord service: python log_discord_messages.py &
Sep 20 05:17:33 PM  gunicorn --threads=2 stigsite.wsgi:application
Sep 20 05:17:38 PM  2022-09-20 08:17:38,019:WARNING - PyNaCl is not installed, voice will NOT be supported
Sep 20 05:17:38 PM  2022-09-20 08:17:38,021:INFO - logging in using static token
Sep 20 05:17:38 PM  2022-09-20 08:17:38,525:INFO - Shard ID None has sent the IDENTIFY payload.
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [55] [INFO] Starting gunicorn 20.1.0
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [55] [INFO] Listening at: http://0.0.0.0:10000 (55)
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [55] [INFO] Using worker: gthread
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [58] [INFO] Booting worker with pid: 58
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [60] [INFO] Booting worker with pid: 60
```
