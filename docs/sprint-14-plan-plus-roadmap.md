## Sprint-14

deploy main Thurs Nov 17

[Ronen's Notion](https://www.notion.so/m4co/Dev-sync-b343131cef7844edae20684fc17bb113)

### HIGH PRIORITY

- [x] G4-3: On **"Team Preferences"** page add toggle widget to turn off/on Twitter handle request message
  - [x] Query the `models.TeamProfile.enable_twitter_handle_request` field within `log_discord_messages.py` and enforce it.
  - [x] Merge staging into main and deploy to TEC


### Medium Priority

- [x] G0.5-0.2: Site FK in discord_message table
- [x] G1-3: Assign all discord_message records to the DAOStack/Stigsite Site id
- [ ] Assign all TEC discord_message records to the TEC Site id
- [ ] messages to DAOStack DB with the TEC Site id
- [x] G1-1.5: Home page focused on **["Join Stigflow"]** button that redirects to login page
- [x] G2-1.5: Hamburger menu on home page
  * [x] Hamburger menu item About
  * [x] Hamburger menu item Privacy Policy
  * [x] Hamburger menu item Terms of Service
  * [x] Hamburger menu item Join Stifflo -> Login
  * [x] Hamburger menu item for "Profile" (user profile) [visible after login]
  * [x] Change the "mode" button to an icon
- [x] G1-1.5: On user Profile page (urls+views.py model-based view for `User`) add button for **"Team Preferences"** (and associated urls+views.py)
- [ ] change url for team visible to user to be better `__str/repr__` representation (name of Team/Community)
- [ ] downgrade all `admin` users to `staff` (shahar->stigsite-dogfood, gideon->stigsite-tec, greg-staff->your-test-team-name) leave hobson, greg-admin, ronen, maria as `admin`
  * [x] dogfood server
  * TEC server

- [ ] **"Team Preferences"** page has editable list of teammates/members:
  - [ ] JsonField list of strs similar to your whitelist code with discord usernames
- [x] G1-0.5: allow `staff` to see a **"Team Preferences"** button on their User Profile page
- [ ] OPTIONAL: move DISCORD_API_KEY from .env to stigsite.models.TeamProfile (from Site base class) field change url for team visible to user to be better `__str/repr__` representation (name of Team/Community)

### DAOStack Tasks

- [ ] RS: Get name for `LICENSE.txt` Copyright (c) for Incubator Named "Safe LLC/Inc/Corp" 
- [ ] RS: EULA - Terms of Service
- [ ] RS: Privacy Policy
- [ ] RS: home page copy + layout + graphic design for "Join Stigflow" page/funnel

