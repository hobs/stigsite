# Setting up a Discord Bot

1) Log in to [Discord](https://discord.com/login).

2) Go to the [Discord Developer Portal](https://discord.com/developers/applications).

3) Click *New Application*.

4) Give the chatbot a name.  Click *Create*.

5) Copy the *PUBLIC KEY*.  You should put this in the .env file.

6) From the sidebar, click **Bot**.

7) Click **Add Bot**.  Then click, **Yes, do it!**.

8) From the sidebar, click **OAuth2**.

9) Select **URL Generator**.

10) In SCOPES, select **bot**.

11) In BOT PERMISSIONS, select all the checkboxes under TEXT PERMISSIONS.

12) Click **Copy**.

13) Visit the URL in your browser.

14) Select the server you want the bot to work in. Click **Continue**.  Then, click **Authorize**.
