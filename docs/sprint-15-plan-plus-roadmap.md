## Sprint-15 Plan Plus Roadmap

### Bugs

- a message not appearing in TopM GardenM nor Airtables: https://discord.com/channels/1001036228294094909/1026566677288189972/1039938408375652392 

## HIGH PRIORITY

- [ ] change url for team visible to user to be better `__str/repr__` representation (name of Team/Community)
- [ ] downgrade gideon->stigsite-tec during data import/migration of TEC messages and other tables 
- [x] **"Team Preferences"** page has editable list of teammates/members:
  - [ ] JsonField list of strs similar to your whitelist code with discord usernames
- [ ] OPTIONAL: move DISCORD_API_KEY from .env to stigsite.models.TeamProfile (from Site base class) field change url for team visible to user to be better `__str/repr__` representation (name of Team/Community)

## HTML templates and Django nice to haves (Rochdi)

No use of the word "user" anywhere.

- R: style form for User Profile
- R: style forms for Community Profile
- R: Change prompt on form to say: "Discord account:"
- R: Change prompt on form to say: "Twitter account:"
- R: Delete user profile file "Twitter username" (redundant with handle)

### community profile

- R: community profile page allows upload of avatar/logo
- R: CSm logo replaced with Site.logo image that community manager/admin uploaded 
- move url to community profile page
- Airtable keys etc 
- Community name field:
- home page footer: "{community name} powered by StigFlow"
- bot name registered with Discord named "StigBot"
- community icon/avatar: upload image

### home page / Top Messages / Garden Messages

- R: remove [Test export] button
- R: dynamic community name in template to replace (TEC Community) in footer on homepage, etc
- R: homepage and other Footers: "powered by StigFlow" instead of "powered by DAOStack"

### Testing (staff user) deploy.md process

Do these after DAO Stack provides copy for TOC and privacy

- [ ] R: Go through process of creating a new Discord App of type "bot" named "Stigbot"
- [ ] R: Add URL to discord form for T.O.S.
- [ ] R: Add URL to discord form for Privacy Policy

### DAOStack Tasks

- [ ] RS: Get name for `LICENSE.txt` Copyright (c) for Incubator Named "Safe LLC/Inc/Corp" 
- [ ] RS: EULA - Terms of Service
- [ ] RS: Privacy Policy
- [ ] RS: home page copy + layout + graphic design for "Join Stigflow" page/funnel


## Done
- [x] downgrade all `admin` users to `staff` (shahar->stigsite-dogfood, greg-staff->your-test-team-name) leave hobson, greg-admin, ronen, maria as `admin`
- [x] G1-0.5: allow `staff` to see a **"Team Preferences"** button on their User Profile page

## Sprint-15 (outdated ideas)

deploy main Thurs Nov 24


## Sprint-16

deploy main Thurs Dec 1

### Testing (staff user) deploy.md process
- [ ] Go through process of creating a new Discord App of type "bot" named "Stigbot"
- [ ] Add URL to discord form for T.O.S.
- [ ] Add URL to discord form for Privacy Policy


- [ ] hamburger menu item **"Team Preferences"**
    - [ ] OPTIONAL: Don't ask for twitter handles for anyone not a community manager (`staff`)

- [ ] Hamburger menu item for logout [visible after login]
- [x] **"Team Preferences"** page with list of teammates:
  - [x] JsonField list of strs similar to your whitelist code with discord usernames
  - [ ] OPTIONAL: `ManyToMany` FK from `TeamProfile` to User`
- [ ] Discord keys in Community Profile page
  - [ ] add some more `DISCORD_*` variables to "Team Preferences" `views.py`
- [ ] Airtable keys in Community Profile page
  - [ ] add some more `AIRTABLE_*` variables to "Team Preferences" `views.py`
- [ ] OPTIONAL: G4: Script to copy all records from simplest TEC database table to dogfood database
- [ ] OPTIONAL: G8: Get more data copied over to dogfood database
- [ ] OPTIONAL: G8: Script to copy all records from simplest TEC database table to dogfood database


## Sprint-17

deploy main Thurs Dec 8

- [ ] Forum Post Tags recorded in `discord_message` table (waiting on PyCord implementation of Discord API)

## Sprint-18

deploy main Thurs Dec 15

- [ ] DISCORD USER: `"/notify me whenever someone talks about a job opportunity"`
    - [ ] natural language queries of discord_messages table with conversational interface
    - [ ] Discord "/search" command ?


## Sprint-19 External Beta Testing

deploy main Thurs Dec 22

- [ ] Apply for listing on Discord Apps Directory

## Sprint-20 MVP Public Beta launch

deploy main Thurs Dec 29

- [ ] Publish trackable tinyurl invitation link on Linked-In
- [ ] Publish trackable tinyurl invitation link on Mastodon
- [ ] Publish trackable tinyurl invitation link on Twitter
- [ ] Publish trackable tinyurl invitation link on TEC Discord
- [ ] Publish trackable tinyurl invitation link on SDPUG Discord
- [ ] Publish trackable tinyurl invitation link on popular subreddit for product/bot launches
- [ ] Publish trackable tinyurl on popular public Discord server
- [ ] Publish trackable tinyurl invitation link on SDML Slack
- [ ] Ask DAOStack, TEC, and partners' employees to publish trackable tinyurl invitation link on all Discord servers

### Dream Features 

- [ ] Sprint 17? (Dec 15) private beta launch
- [ ] Integrated with Zapier
- [ ] Rule system within django Views similar to Zapier?
- [ ] Web3 DAPs?


### Testing (staff user) deploy.md process
- [ ] Go through process of creating a new Discord App of type "bot" named "Stigbot"
- [ ] Add URL to discord form for T.O.S.
- [ ] Add URL to discord form for Privacy Policy


- [ ] hamburger menu item **"Team Preferences"**
    - [ ] OPTIONAL: Don't ask for twitter handles for anyone not a community manager (`staff`)

- [ ] Hamburger menu item for logout [visible after login]
- [x] **"Team Preferences"** page with list of teammates:
  - [x] JsonField list of strs similar to your whitelist code with discord usernames
  - [ ] OPTIONAL: `ManyToMany` FK from `TeamProfile` to User`
- [ ] Discord keys in Community Profile page
  - [ ] add some more `DISCORD_*` variables to "Team Preferences" `views.py`
- [ ] Airtable keys in Community Profile page
  - [ ] add some more `AIRTABLE_*` variables to "Team Preferences" `views.py`
- [ ] OPTIONAL: G4: Script to copy all records from simplest TEC database table to dogfood database
- [ ] OPTIONAL: G8: Get more data copied over to dogfood database
- [ ] OPTIONAL: G8: Script to copy all records from simplest TEC database table to dogfood database
