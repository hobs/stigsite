# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    id = models.BigAutoField(primary_key=True)
    avatar = models.DateTimeField(blank=True, null=True)
    public_flags = models.BigIntegerField(blank=True, null=True)
    flags = models.BigIntegerField(blank=True, null=True)
    locale = models.CharField(max_length=-1, blank=True, null=True)
    mfa_enabled = models.BooleanField(blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    active = models.BooleanField(blank=True, null=True)
    staff = models.BooleanField(blank=True, null=True)
    admin = models.BooleanField(blank=True, null=True)
    email = models.CharField(max_length=-1, blank=True, null=True)
    password = models.CharField(max_length=-1, blank=True, null=True)
    discord_tag = models.CharField(max_length=-1, blank=True, null=True)
    username = models.CharField(max_length=-1, blank=True, null=True)
    first_name = models.CharField(max_length=-1, blank=True, null=True)
    last_name = models.CharField(max_length=-1, blank=True, null=True)
    date_joined = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'auth_user'


class DiscordChannel(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=-1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_channel'


class DiscordGuild(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=-1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_guild'


class DiscordMessage(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    edited_at = models.DateTimeField(blank=True, null=True)
    content = models.CharField(max_length=-1, blank=True, null=True)
    author = models.JSONField(blank=True, null=True)
    reactions = models.JSONField(blank=True, null=True)
    saved_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_message'


class DiscordReaction(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    message_id = models.BigIntegerField(blank=True, null=True)
    user = models.BigIntegerField(blank=True, null=True)
    emoji = models.TextField(blank=True, null=True)
    previous_reactions = models.JSONField(blank=True, null=True)
    previous_reaction_users = models.JSONField(blank=True, null=True)
    users = models.JSONField(blank=True, null=True)
    discord_message = models.ForeignKey(DiscordMessage, models.DO_NOTHING, db_column='discord_message', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_reaction'


class DiscordUser(models.Model):
    id = models.BigAutoField(primary_key=True)
    discord_tag = models.CharField(max_length=-1, blank=True, null=True)
    avatar = models.CharField(max_length=-1, blank=True, null=True)
    public_flags = models.BigIntegerField(blank=True, null=True)
    flags = models.BigIntegerField(blank=True, null=True)
    locale = models.CharField(max_length=-1, blank=True, null=True)
    mfa_enabled = models.BooleanField(blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    active = models.BooleanField(blank=True, null=True)
    staff = models.BooleanField(blank=True, null=True)
    admin = models.BooleanField(blank=True, null=True)
    email = models.CharField(max_length=-1, blank=True, null=True)
    password = models.CharField(max_length=-1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_user'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class GardenMessage(models.Model):
    id = models.BigAutoField(primary_key=True)
    original_message = models.TextField(blank=True, null=True)
    reactions = models.JSONField(blank=True, null=True)
    reaction_count = models.FloatField(blank=True, null=True)
    emoji_list = models.JSONField(blank=True, null=True)
    user_count = models.BigIntegerField(blank=True, null=True)
    user_list = models.JSONField(blank=True, null=True)
    user_id = models.CharField(max_length=-1, blank=True, null=True)
    author = models.JSONField(blank=True, null=True)
    message_id = models.BigIntegerField(blank=True, null=True)
    editable_message = models.TextField(blank=True, null=True)
    unique_reaction_count = models.BigIntegerField(blank=True, null=True)
    unique_emoji_list = models.JSONField(blank=True, null=True)
    unique_user_count = models.BigIntegerField(blank=True, null=True)
    unique_user_list = models.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'garden_message'


class Log(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    json = models.JSONField(blank=True, null=True)
    text = models.CharField(max_length=-1, blank=True, null=True)
    log_level = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'log'


class StigappRule(models.Model):
    id = models.BigAutoField(primary_key=True)
    rule_name = models.CharField(max_length=255, blank=True, null=True)
    chosen_reactions = models.JSONField(blank=True, null=True)
    channel_name = models.CharField(max_length=255, blank=True, null=True)
    role = models.CharField(max_length=255, blank=True, null=True)
    threshold = models.FloatField(blank=True, null=True)
    activated_status = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'stigapp_rule'


class TopMessage(models.Model):
    id = models.BigAutoField(primary_key=True)
    original_message = models.TextField(blank=True, null=True)
    reactions = models.JSONField(blank=True, null=True)
    reaction_count = models.FloatField(blank=True, null=True)
    emoji_list = models.JSONField(blank=True, null=True)
    user_count = models.BigIntegerField(blank=True, null=True)
    user_list = models.JSONField(blank=True, null=True)
    added_to_garden = models.BooleanField(blank=True, null=True)
    author = models.JSONField(blank=True, null=True)
    message_id = models.BigIntegerField(blank=True, null=True)
    unique_reaction_count = models.BigIntegerField(blank=True, null=True)
    unique_emoji_list = models.JSONField(blank=True, null=True)
    unique_user_count = models.BigIntegerField(blank=True, null=True)
    unique_user_list = models.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'top_message'
