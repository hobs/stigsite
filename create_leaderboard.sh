#!/usr/bin/env bash
python manage.py shell << EOF
from scripts.create_leaderboard_data import *
up = manage_leaderboard_data()
up.generate_leaderboard_data()
exit()
EOF
