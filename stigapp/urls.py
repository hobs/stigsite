from django.urls import path

from .views import (
    DiscordMessageListView,
    DiscordReactionListView,
    GardenMessageListView,
    TopMessageListView,
    # LeaderBoardView,
    home,
    about,
    page_not_found_404_view,
    export_csv,
    export_to_airtable,
    terms,
    privacy,
    # export_top_message_csv,
    # export_garden_message_csv,
    update_top_message_list,

)

urlpatterns = [
    # path('', home, name='home'),
    path('', home, name='home'),
    path('terms', terms, name='terms'),
    path('privacy', privacy, name='privacy'),
    path('garden-messages', GardenMessageListView.as_view(), name='garden_messages'),
    path('about', about, name='about'),
    path('top-messages', TopMessageListView.as_view(), name='top_messages'),
    # path('leaderboard', LeaderBoardView.as_view(), name='leaderboard-list'),
    # path('leaderboard/messages', DiscordMessageListView.as_view(), name='message-list'),
    # path('leaderboard/reactions', DiscordReactionListView.as_view(), name='reaction-list'),
    path('export_csv/<str:table>/<str:selected>/<str:test>', export_csv, name='export_csv'),
    path('export-airtable', export_to_airtable, name='export_to_airtable'),
    # path('export-tm', export_top_message_csv, name='export_topmessage'),
    # path('export-gm', export_garden_message_csv, name='export_gardenmessage'),
    path('update-tm', update_top_message_list, name='update_topmessage'),
]
