import config
import datetime
import os
import pytz
import random

from pyairtable import Table
from dotenv import load_dotenv
# from .models import GardenMessage
from supabase import create_client
SUPA = create_client(config.SUPABASE_URL, config.SUPABASE_KEY)


load_dotenv()


class AirTableServiceSupabase:
    """ Exports the current entries in the TopMessage table to the Airtabled linked to Stigsite through .env

    str(Message_id) is necessary to avoid adding 00 to message ids on creation in Airtable

    NOTE: The API is limited to 5 requests per second
    """

    def __init__(self):
        self._api_key = config.AIRTABLE_API_KEY
        self._airtable_base_id = config.AIRTABLE_BASE_ID
        self._airtable_table_name_top_messages = config.AIRTABLE_TABLE_NAME_TOP_MESSAGES
        self._airtable_table_name_garden = config.AIRTABLE_TABLE_NAME_GARDEN

    def build_field_backwards_compatibility_object(self, message):
        additional_fields = {
            "Authorship": message['created_at'],
            "Channel": message['channel_name'],
            "Guild": message['guild_name'],
            "Editable Message": message['editable_message'],
            "Edited At": message['edited_at'],
            "Extracted URL": self.get_extracted_url_value(message),
            "Thread Title": message.get('message_thread_title', '')
        }
        return additional_fields

    def check_for_existing_record(self, table, message):
        airtable_entries = table.all()
        airtable_message_ids = []
        for entry in airtable_entries:
            try:
                airtable_message_ids.append(entry['fields']['Message ID'])
            except KeyError:
                pass

        test = str(message['message_id']) in airtable_message_ids
        return test

    def delete_airtable_entry(self, table_name, message_id):
        table = Table(self._api_key, self._airtable_base_id, self._airtable_table_name_top_messages)
        airtable_entries = table.all()
        airtable_message_id = ''

        for entry in airtable_entries:
            try:
                if entry['fields']['Message ID'] == str(message_id):
                    airtable_message_id = entry['id']
            except KeyError:
                pass

        table.delete(airtable_message_id)

    def update_or_create_a_single_record(self, table_name, message):
        print("AIRTABLE MESSAGE: MESSAGE==============")
        print(message)
        print("=======================================")

        if table_name == self._airtable_table_name_top_messages:
            table = Table(self._api_key, self._airtable_base_id, self._airtable_table_name_top_messages)
        else:
            table = Table(self._api_key, self._airtable_base_id, self._airtable_table_name_garden)

        existing_record_boolean = self.check_for_existing_record(table, message)

        if existing_record_boolean is True:
            self.update_a_single_leaderboard_entry(table, message)
        else:
            self.create_a_single_leaderboard_entry(table_name, table, message)

    def get_export_datetimetz(self):
        tz = pytz.timezone(config.TIME_ZONE)
        time_info = tz.localize(datetime.datetime.now())
        return time_info

    def chunker(self, seq, size):
        return(seq[pos:pos + size] for pos in range(0, len(seq), size))

    def bulk_add_leaderboard(self):
        # Get the table
        table = Table(self._api_key, self._airtable_base_id, self._airtable_table_name_garden)

        # Get the current GardenMessages in Airtable
        airtable_entries = table.all()
        airtable_message_ids = []
        for entry in airtable_entries:
            try:
                airtable_message_ids.append(entry['fields']['Message ID'])
            except KeyError:
                pass

        # Get the current GardenMessages in Supabase
        # messages = TopMessage.objects.all()
        # Get the current GardenMessages in Supabase
        messages_response = SUPA.table('garden_message').select('*').execute()
        messages = messages_response.data

        messages_to_be_added = []
        for message in messages:
            # Check if a GardenMessage already exists in Airtable
            if str(message['message_id']) not in airtable_message_ids:
                message_object = self.make_garden_message_data_object(message)
                # Add entry to list
                messages_to_be_added.append(message_object)

        for group in self.chunker(messages_to_be_added, 5):
            table.batch_create(group)

    def make_reaction_object(self, message):
        user_list = []
        for user in message['user_list']:
            user_list.append(f"{user['name']}#{user['discriminator']}")
        user_list_str = ', '.join(user_list)
        unique_user_list = set(user_list.copy())
        unique_user_list_str = ', '.join(unique_user_list)

        emoji_counts = ''
        for k, v in message['counts_by_emoji'].items():
            emoji_counts += f'{k}: {v}\n'

        reaction_data = {
            'user_list_str': user_list_str,
            'unique_user_list_str': unique_user_list_str,
            'emoji_counts': emoji_counts
        }
        return reaction_data

    def get_extracted_url_value(self, message):
        extracted_urls_query_result = SUPA.table('extracted_urls').select('*').eq('id', message['message_id']).execute()
        extracted_url = extracted_urls_query_result.data[0]['url_list'][0] if extracted_urls_query_result.data != [] else ''
        return extracted_url

    def make_top_message_data_object(self, message):
        reaction_data = self.make_reaction_object(message)

        message_data = {
            "Author": message['author_discord_username'],
            "Twitter Handle": message['twitter_handle'],
            "Guild": message['guild_name'],
            "Channel": message['channel_name'],
            "Message ID": str(message['message_id']),
            "Message Type": message['message_type'],
            "Original Message": message['original_message'],
            "Authorship": message['authorship_datetime'],
            "Editable Message": message['editable_message'],
            "Edited At": str(message['edited_at']) if message['edited_at'] else None,

            "All Reactions": message['reaction_count'],
            "Unique Reactions": message['unique_reaction_count'],
            "All Emojis": ', '.join(message['emoji_list']),
            "Unique Emojis": ', '.join(message['unique_emoji_list']),
            "Totals by Emoji": reaction_data['emoji_counts'],
            "Total Users": message['user_count'],
            "Unique Users": message['unique_user_count'],
            "Total User List": reaction_data['user_list_str'],
            "Unique User List": reaction_data['unique_user_list_str'],
            "Added to Garden": message['added_to_garden'],
            "Exported At": str(self.get_export_datetimetz()),
            "Jump URL": message['jump_url'],
            "Extracted URL": self.get_extracted_url_value(message)
        }
        return message_data

    def make_garden_message_data_object(self, message):
        reaction_data = self.make_reaction_object(message)
        message_data = {
            "Author": message['author_discord_username'],
            "Twitter Handle": message['twitter_handle'],
            "Guild": message['guild_name'],
            "Channel": message['channel_name'],
            # Airtable was adding 00 to each id
            "Message ID": str(message['message_id']),
            "Message Type": message['message_type'],
            "Original Message": message['original_message'],
            "Authorship": message['authorship_datetime'],
            "Editable Message": message['editable_message'],
            "Edited At": str(message['edited_at']) if message['edited_at'] else None,
            "All Reactions": message['reaction_count'],
            "Unique Reactions": message['unique_reaction_count'],
            "All Emojis": ', '.join(message['emoji_list']),
            "Unique Emojis": ', '.join(message['unique_emoji_list']),
            "Totals by Emoji": reaction_data['emoji_counts'],
            "Total Users": message['user_count'],
            "Unique Users": message['unique_user_count'],
            "Total User List": reaction_data['user_list_str'],
            "Unique User List": reaction_data['unique_user_list_str'],
            "Exported At": str(self.get_export_datetimetz()),
            "Jump URL": message['jump_url'],
            "Extracted URL": self.get_extracted_url_value(message)
        }
        return message_data

    def create_a_single_leaderboard_entry(self, table_name, table, message):

        if table_name == self._airtable_table_name_top_messages:
            message_data = self.make_top_message_data_object(message)
        else:
            message_data = self.make_garden_message_data_object(message)

        table.create(message_data)

    # def update_a_single_leaderboard_entry(self, message_id, reaction_data):
    def update_a_single_leaderboard_entry(self, table, message):
        airtable_entries = table.all()
        airtable_message_id = ''

        reaction_data = self.make_reaction_object(message)

        backwards_compatibility_object = self.build_field_backwards_compatibility_object(message)

        for entry in airtable_entries:
            try:
                if entry['fields']['Message ID'] == str(message['message_id']):
                    airtable_message_id = entry['id']
            except KeyError:
                pass

        reaction_data = {
            # "Editable Message": message['editable_message'],
            # "Edited At": str(message['edited_at']) if message['edited_at'] else None,
            "All Reactions": message['reaction_count'],
            "Unique Reactions": message['unique_reaction_count'],
            "All Emojis": ', '.join(message['emoji_list']),
            "Unique Emojis": ', '.join(message['unique_emoji_list']),
            "Totals by Emoji": reaction_data['emoji_counts'],
            "Total Users": message['user_count'],
            "Unique Users": message['unique_user_count'],
            "Total User List": reaction_data['user_list_str'],
            "Unique User List": reaction_data['unique_user_list_str'],
        }

        reaction_data.update(backwards_compatibility_object)

        # Need to add add_to_garden because sometimes, the value for this will change between interaction data
        if table.table_name == self._airtable_table_name_top_messages:
            reaction_data['Added to Garden'] = message['added_to_garden']

        table.update(
            airtable_message_id,
            reaction_data
        )
