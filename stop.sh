#!/usr/bin/env bash
# stop.sh
PID2KILL=$(ps aux | grep 'python log_' | cut -c10-18 | head -n 1)
echo "Killing $PID2KILL..."
kill -9 $PID2KILL