import hashlib
import time
import datetime

from django.contrib.auth.models import (
    BaseUserManager,
    UserManager,
    AbstractBaseUser,
    AbstractUser,
)
from django.contrib.sites.models import Site
from django.db import models
from django.utils import timezone
from django.urls import reverse


################################################################
# Community profile settings
class CommunityProfile(models.Model):
    id = models.BigAutoField(primary_key=True)
    airtable_credentials = models.JSONField(
        blank=True,
        null=True,
        help_text="The Airtable key, base name, and table name associated with Stigsite"
    )
    monitored_channels = models.JSONField(
        blank=True,
        null=True,
        help_text="A list of channels within guilds the site will listen to"
    )
    monitored_guilds = models.JSONField(
        blank=True,
        null=True,
        help_text="A list of Discord guilds (servers) the site will listen to"
    )
    promotion_threshold = models.IntegerField()
    site_style = models.JSONField(
        blank=True,
        null=True,
        help_text="The Airtable key, base name, and table name associated with Stigsite"
    )
    django_site_id = models.ForeignKey(
        Site,
        db_column='django_site_id',
        related_name="+",
        on_delete=models.CASCADE,
        null=True
    )
    enable_twitter_handle_request = models.BooleanField(default=False)

    # extracted_urls_id = models.ForeignKey(
    #     "ExtractedUrls",
    #     db_column='extracted_urls_id',
    #     # related_name='extracted_urls',
    #     on_delete=models.CASCADE,
    #     help_text="The id of the EmbeddedMessage table entry with the message's extracted URLS"
    # )

    class Meta:
        managed = False
        db_table = 'community_profile'

    def get_absolute_url(self):
        return reverse('community-profile', kwargs={'pk': self.pk})


class UserManager(UserManager):

    def check_for_admin_authorization(self, discord_tag):
        """ Checks against a hardcoded list to see if a user is approved for admin privileges"""

        check = False
        approved_admin_users = [
            # 'insert discord_tags',
            'thompsgj#8407',
            'hobs#3386',
            'hobs#9238',
            'Sha#6179',
            'ronent#2267',
            'mariadyshel#6267',
            'richa#0775',
            'GideonRo#3175',
            'Irem#3362',
            'enti#1546',
            'bear100#9085',
            'acidlazzer#5796',
        ]
        if discord_tag in approved_admin_users:
            check = True

        return check

    def create_password(self):
        """  Makes a string for the mandatory, non-nullable password field.

        The password will not work even if a user knows it because authentication happens through Discord.
        """
        hash = hashlib.sha1()
        hash.update(str(time.time()).encode('utf-8'))
        return hash.hexdigest()[:7]

    def create_new_discord_user(self, user, password=None):
        """ Creates a user in the DiscordUser table.

        Uses the 'DiscordAuthenticationBackend' for authentication, not the standard Django auth backend.
        """

        discord_tag = '%s#%s' % (user['username'], user['discriminator'])
        permissions_boolean = self.check_for_admin_authorization(discord_tag)

        new_user = self.model(
            email='test@test.com'
        )
        new_user.id = user['id']
        new_user.username = discord_tag
        new_user.discord_tag = discord_tag
        new_user.twitter_handle = ''
        new_user.twitter_username = ''
        new_user.avatar = user['avatar']
        new_user.public_flags = user['public_flags']
        new_user.flags = user['flags']
        new_user.locale = user['locale']
        new_user.mfa_enabled = user['mfa_enabled']
        new_user.active = True
        new_user.staff = permissions_boolean
        new_user.admin = permissions_boolean

        password = self.create_password()
        new_user.set_password(password)
        new_user.save(using=self._db)
        return new_user


class User(AbstractUser):
    """ A user who has logged in to stigsite through Discord """

    _DATABASE = 'supabase'

    objects = UserManager()

    # Discord-specific user attributes
    id = models.BigIntegerField(primary_key=True)
    username = models.CharField(max_length=100, unique=True)
    discord_tag = models.CharField(max_length=100, unique=True)
    twitter_handle = models.CharField(max_length=100, blank=True, null=True)
    twitter_username = models.CharField(max_length=100, blank=True, null=True)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    avatar = models.CharField(blank=True, null=True, max_length=100)
    public_flags = models.IntegerField()
    flags = models.IntegerField()
    locale = models.CharField(max_length=100)
    mfa_enabled = models.BooleanField()
    date_joined = models.DateTimeField(default=timezone.now)
    last_login = models.DateTimeField(default=timezone.now)

    django_site_id = models.ForeignKey(
        Site,
        verbose_name="Team URL",
        db_column='django_site_id',
        related_name="+",
        on_delete=models.CASCADE,
        null=True
    )

    # Django admin user attributes
    # authenticated = models.BooleanField(default=True)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=True)
    admin = models.BooleanField(default=True)
    email = models.CharField(max_length=255, default="test@test.com")
    requested_twitter_handle = models.BooleanField(default=False)

    USERNAME_FIELD = 'discord_tag'
    REQUIRED_FIELDS = []

    class Meta:
        managed = False
        db_table = 'auth_user'
        # app_label = 'user_data'

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.discord_tag

    def get_short_name(self):
        return self.discord_tag

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_active(self):
        return self.active

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_superuser(self):
        return self.admin

    @property
    def is_authenticated(self):
        return True

    def get_absolute_url(self):
        return reverse('profile', kwargs={'pk': self.pk})


# class DiscordUserManager(BaseUserManager):

#     def check_for_admin_authorization(self, discord_tag):
#         """ Checks against a hardcoded list to see if a user is approved for admin privileges"""

#         check = False
#         approved_admin_users = [
#             # 'insert discord_tags',
#             'thompsgj#8407'
#         ]
#         if discord_tag in approved_admin_users:
#             check = True

#         return check

#     def create_password(self):
#         """  Makes a string for the mandatory, non-nullable password field.

#         The password will not work even if a user knows it because authentication happens through Discord.
#         """
#         hash = hashlib.sha1()
#         hash.update(str(time.time()).encode('utf-8'))
#         return hash.hexdigest()[:7]

#     def create_new_discord_user(self, user, password=None):
#         """ Creates a user in the DiscordUser table.

#         Uses the 'DiscordAuthenticationBackend' for authentication, not the standard Django auth backend.
#         """

#         discord_tag = '%s#%s' % (user['username'], user['discriminator'])
#         permissions_boolean = self.check_for_admin_authorization(discord_tag)

#         new_user = self.model(
#             email='test@test.com'
#         )
#         new_user.id = user['id']
#         new_user.discord_tag = discord_tag
#         new_user.avatar = user['avatar']
#         new_user.public_flags = user['public_flags']
#         new_user.flags = user['flags']
#         new_user.locale = user['locale']
#         new_user.mfa_enabled = user['mfa_enabled']
#         new_user.active = True
#         new_user.staff = permissions_boolean
#         new_user.admin = permissions_boolean

#         password = self.create_password()
#         new_user.set_password(password)
#         new_user.save(using=self._db)
#         return new_user


# class DiscordUser(AbstractBaseUser):
#     """ A user who has logged in to stigsite through Discord """

#     _DATABASE = 'supabase'

#     objects = DiscordUserManager()

#     # Discord-specific user attributes
#     id = models.BigIntegerField(primary_key=True)
#     discord_tag = models.CharField(max_length=100, unique=True)
#     avatar = models.CharField(blank=True, null=True, max_length=100)
#     public_flags = models.IntegerField()
#     flags = models.IntegerField()
#     locale = models.CharField(max_length=100)
#     mfa_enabled = models.BooleanField()
#     last_login = models.DateTimeField(null=True)

#     # Django admin user attributes
#     # authenticated = models.BooleanField(default=True)
#     active = models.BooleanField(default=True)
#     staff = models.BooleanField(default=True)
#     admin = models.BooleanField(default=True)
#     email = models.CharField(max_length=255, default="test@test.com")

#     USERNAME_FIELD = 'discord_tag'
#     REQUIRED_FIELDS = []

#     class Meta:
#         managed = False
#         db_table = 'discord_user'

#     def __str__(self):
#         return self.email

#     def get_full_name(self):
#         return self.discord_tag

#     def get_short_name(self):
#         return self.discord_tag

#     def has_perm(self, perm, obj=None):
#         return True

#     def has_module_perms(self, app_label):
#         return True

#     @property
#     def is_active(self):
#         return self.active

#     @property
#     def is_staff(self):
#         return self.staff

#     @property
#     def is_admin(self):
#         return self.admin

#     @property
#     def is_authenticated(self):
#         return True
