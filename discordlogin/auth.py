from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User

# from .models import DiscordUser
from .models import User as DiscordUser


class DiscordAuthenticationBackend(BaseBackend):
    def authenticate(self, request, user) -> DiscordUser:
        """ Called when Django uses authentication in views.discord_login_redirect

        If user is new, saves their info to DiscordUser.

        Returns the authenticated user's information
        """
        find_user = DiscordUser.objects.filter(id=user['id'])

        if len(find_user) == 0:
            print('User was not found.  Saving...')

            new_user = DiscordUser.objects.create_new_discord_user(user)
            print(user)
            return new_user
        print("User was found. Returning...")
        return find_user

    def get_user(self, user_id):
        """ Necessary to move through redirect to the authenticate page """
        try:
            return DiscordUser.objects.get(pk=user_id)
        except DiscordUser.DoesNotExist:
            return None
