def get_coroutine(obj, attr_path='channel.send'):
    """ Same functionality as getdottedattr? """
    coro = obj
    for attr in attr_path.split('.'):
        coro = getattr(coro, attr)
    return coro


async def await_action(message, action='channel.send', *args, **kwargs):
    await get_coroutine(message, action)(*args, **kwargs)


def flatten_coroutine(obj, attr):
    return await_action(obj, attr)().flatten()
