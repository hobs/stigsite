# utils.py
# isoformat(datatime)
import datetime


def getdottedattr(obj, attr, default=None):
    """ Same functionality as get_coroutine? """
    child_obj = obj
    attr = attr.strip().strip('.')  # in case someone calls this with a an attr named '.whatever.child.name'
    children = [attr]

    if '.' in attr:
        children = attr.split('.')
    for name in children[:-1]:
        child_obj = getattr(child_obj, name, default)
    return getattr(child_obj, children[-1], default)


def to_dict(obj, attrs):
    """ Convert an obj to a dictionary using getattr(obj, key, None) for each attr (key) in attrs """
    return {a.strip().strip('.').replace('.', '__'): getdottedattr(obj, a, None) for a in attrs}


def to_listodicts(objs, attrs, coros=()):
    list_of_dicts = [to_dict(obj=obj, attrs=attrs) for obj in objs]
    return list_of_dicts
    # for coro in coros:
    #     for d, obj in zip(list_of_dicts, objs):
    #         d[coro] = flatten_coroutine(obj, coro)


def isoformat(dt=None):
    if dt is None or not dt:
        return datetime.datetime.now()
    return getattr(dt, 'isoformat', lambda x: None)()
